import React from "react";
import { render } from "react-dom";
import { ThemeProvider } from "@chakra-ui/core";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import {
	BrowserRouter as Router,
	Routes,
	Route
} from "react-router-dom";

import Header from "./components/Header";
import PowerSupplies from "./components/PowerSupplies";
import Chillers from "./components/Chillers";
import Tabbing from "./components/Tabbing";

function App() {
  return (
    <ThemeProvider>
      <Header />
      <Tabbing />
    </ThemeProvider>
  );
}

const rootElement = document.getElementById("root");
render(
	<Router>
		<App />
	</Router>,
	rootElement);
