import React from "react";

export default function Home() {
	return (
		<>
		<hr></hr>
		  <h1 style={{textAlign:"center"}}>Home</h1>
		<hr></hr>
			<main>
				<h2>Welcome to the GUI for Accessing LabRemote Equipment!</h2>
				<p>This app is intended to aide in user-friendly control of hardware such as chillers and power supplies both in the lab and remotely. It is compatible with all browsers and operating systems, and this app can be customized for the needs of any particular lab. It relies on a FastAPI backend and a React App frontend.</p>
				<p>The main requirement for use is that the lab hardware systems can establish communication via labRemote libraries. With the use of Docker, no software dependencies are necessary.</p>
				<p>More information and complete documentation can be found <a href="https://gitlab.cern.ch/scipp/pixels/gale">here</a>. This app was built by and is actively maintained by Hava Schwartz. Do not hesitate to reach out with questions regarding installation, use, or customization by emailing hrschwar@ucsc.edu.</p>
			</main>
		</>
	);
}
