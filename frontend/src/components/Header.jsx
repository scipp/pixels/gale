import React from "react";
import { Heading, Flex, Divider } from "@chakra-ui/core";

const Header = () => {
  return (
    <Flex
      as="nav"
      align="center"
      justify="space-between"
      wrap="wrap"
      padding="0.5rem"
      bg="orange.400"
      style={{justifyContent:"center"}}
    >
      <Flex mr={5}>
        <Heading as="h1" size="l">
          GUI for Accessing LabRemote Equipment (GALE)
        </Heading>
        <Divider />
      </Flex>
    </Flex>
  );
};

export default Header;
